# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<Aims>` | `<aims>` (components/Aims.vue)
- `<Footer>` | `<footer>` (components/Footer.vue)
- `<Join>` | `<join>` (components/Join.vue)
- `<Nav>` | `<nav>` (components/Nav.vue)
- `<Network>` | `<network>` (components/Network.vue)
- `<News>` | `<news>` (components/News.vue)
- `<Newsletter>` | `<newsletter>` (components/Newsletter.vue)
- `<Partners>` | `<partners>` (components/Partners.vue)
- `<Post>` | `<post>` (components/Post.vue)
- `<Share>` | `<share>` (components/Share.vue)
- `<Upload>` | `<upload>` (components/Upload.vue)
