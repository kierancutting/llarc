import Vue from 'vue'
import { library, config } from '@fortawesome/fontawesome-svg-core'
import {
  FontAwesomeLayers,
  FontAwesomeLayersText,
  FontAwesomeIcon
} from '@fortawesome/vue-fontawesome'

import {
  faSearch as freeFasFaSearch
} from '@fortawesome/free-solid-svg-icons'

import {
  faTwitter as freeFabFaTwitter
} from '@fortawesome/free-brands-svg-icons'

library.add(
  freeFasFaSearch,
  freeFabFaTwitter
)

config.autoAddCss = false

Vue.component('FontAwesomeIcon', FontAwesomeIcon)
Vue.component('FontAwesomeLayers', FontAwesomeLayers)
Vue.component('FontAwesomeLayersText', FontAwesomeLayersText)
