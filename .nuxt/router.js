import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _2d10a22f = () => interopDefault(import('../pages/about.vue' /* webpackChunkName: "pages/about" */))
const _2deda6c2 = () => interopDefault(import('../pages/contact.vue' /* webpackChunkName: "pages/contact" */))
const _36ce932a = () => interopDefault(import('../pages/events/index.vue' /* webpackChunkName: "pages/events/index" */))
const _8ea7d210 = () => interopDefault(import('../pages/join.vue' /* webpackChunkName: "pages/join" */))
const _66fa3e58 = () => interopDefault(import('../pages/listen/index.vue' /* webpackChunkName: "pages/listen/index" */))
const _2e20f1e4 = () => interopDefault(import('../pages/news/index.vue' /* webpackChunkName: "pages/news/index" */))
const _57023fcf = () => interopDefault(import('../pages/our-network.vue' /* webpackChunkName: "pages/our-network" */))
const _f5cc9bac = () => interopDefault(import('../pages/privacy.vue' /* webpackChunkName: "pages/privacy" */))
const _442d3baa = () => interopDefault(import('../pages/tags/index.vue' /* webpackChunkName: "pages/tags/index" */))
const _13e1cecf = () => interopDefault(import('../pages/upload.vue' /* webpackChunkName: "pages/upload" */))
const _3517f1e2 = () => interopDefault(import('../pages/events/_slug.vue' /* webpackChunkName: "pages/events/_slug" */))
const _65439d10 = () => interopDefault(import('../pages/listen/_slug.vue' /* webpackChunkName: "pages/listen/_slug" */))
const _2c6a509c = () => interopDefault(import('../pages/news/_slug.vue' /* webpackChunkName: "pages/news/_slug" */))
const _42769a62 = () => interopDefault(import('../pages/tags/_slug.vue' /* webpackChunkName: "pages/tags/_slug" */))
const _4cc87cf4 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))
const _3d639b01 = () => interopDefault(import('../pages/_.vue' /* webpackChunkName: "pages/_" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/about",
    component: _2d10a22f,
    name: "about"
  }, {
    path: "/contact",
    component: _2deda6c2,
    name: "contact"
  }, {
    path: "/events",
    component: _36ce932a,
    name: "events"
  }, {
    path: "/join",
    component: _8ea7d210,
    name: "join"
  }, {
    path: "/listen",
    component: _66fa3e58,
    name: "listen"
  }, {
    path: "/news",
    component: _2e20f1e4,
    name: "news"
  }, {
    path: "/our-network",
    component: _57023fcf,
    name: "our-network"
  }, {
    path: "/privacy",
    component: _f5cc9bac,
    name: "privacy"
  }, {
    path: "/tags",
    component: _442d3baa,
    name: "tags"
  }, {
    path: "/upload",
    component: _13e1cecf,
    name: "upload"
  }, {
    path: "/events/:slug",
    component: _3517f1e2,
    name: "events-slug"
  }, {
    path: "/listen/:slug",
    component: _65439d10,
    name: "listen-slug"
  }, {
    path: "/news/:slug",
    component: _2c6a509c,
    name: "news-slug"
  }, {
    path: "/tags/:slug",
    component: _42769a62,
    name: "tags-slug"
  }, {
    path: "/",
    component: _4cc87cf4,
    name: "index"
  }, {
    path: "/*",
    component: _3d639b01,
    name: "all"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
