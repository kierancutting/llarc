(window.webpackJsonp=window.webpackJsonp||[]).push([[19,8],{288:function(t,e,r){var content=r(291);content.__esModule&&(content=content.default),"string"==typeof content&&(content=[[t.i,content,""]]),content.locals&&(t.exports=content.locals);(0,r(18).default)("67240df6",content,!0,{sourceMap:!1})},289:function(t,e,r){"use strict";t.exports=function(t,e){return e||(e={}),"string"!=typeof(t=t&&t.__esModule?t.default:t)?t:(/^['"].*['"]$/.test(t)&&(t=t.slice(1,-1)),e.hash&&(t+=e.hash),/["'() \t\n]/.test(t)||e.needQuotes?'"'.concat(t.replace(/"/g,'\\"').replace(/\n/g,"\\n"),'"'):t)}},290:function(t,e,r){"use strict";r(288)},291:function(t,e,r){var n=r(17)(!1);n.push([t.i,".primary[data-v-8df0df18]{color:#17333f}.primary-bg[data-v-8df0df18]{background-color:#4282b3}.secondary[data-v-8df0df18]{color:#ecf2f5}.secondary-bg[data-v-8df0df18]{background-color:#ecf2f5}.verty[data-v-8df0df18]{margin:0;position:absolute;top:50%;transform:translateY(-50%)}.post-image[data-v-8df0df18]{max-width:40vw}.tags[data-v-8df0df18]{text-decoration:none}.tags[data-v-8df0df18]:hover{text-decoration:underline}.mixcloud[data-v-8df0df18]{height:30vh}.iframe[data-v-8df0df18]{height:100%}iframe[data-v-8df0df18]{width:100%}div[data-v-8df0df18]  iframe{border:none}article[data-v-8df0df18]:hover{background-color:#ecf2f5}article:hover h2[data-v-8df0df18],article:hover p[data-v-8df0df18]{color:#17333f}.post[data-v-8df0df18]{min-height:30vh}",""]),t.exports=n},292:function(t,e,r){"use strict";r.r(e);r(44),r(74),r(197),r(134),r(47),r(50),r(35),r(8),r(15),r(36),r(19),r(25),r(37),r(38),r(26);var n=r(60),o=r.n(n),l=r(59),c=r.n(l);function d(t,e){var r="undefined"!=typeof Symbol&&t[Symbol.iterator]||t["@@iterator"];if(!r){if(Array.isArray(t)||(r=function(t,e){if(!t)return;if("string"==typeof t)return f(t,e);var r=Object.prototype.toString.call(t).slice(8,-1);"Object"===r&&t.constructor&&(r=t.constructor.name);if("Map"===r||"Set"===r)return Array.from(t);if("Arguments"===r||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(r))return f(t,e)}(t))||e&&t&&"number"==typeof t.length){r&&(t=r);var i=0,n=function(){};return{s:n,n:function(){return i>=t.length?{done:!0}:{done:!1,value:t[i++]}},e:function(t){throw t},f:n}}throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")}var o,l=!0,c=!1;return{s:function(){r=r.call(t)},n:function(){var t=r.next();return l=t.done,t},e:function(t){c=!0,o=t},f:function(){try{l||null==r.return||r.return()}finally{if(c)throw o}}}}function f(t,e){(null==e||e>t.length)&&(e=t.length);for(var i=0,r=new Array(e);i<e;i++)r[i]=t[i];return r}var h={props:["post","path","link","tags","showDate","white"],methods:{getPostDate:function(t){return o()(t.published_at,"mmmm dS, yyyy")},extractLink:function(html){var t=new c.a(html).findAll("a");return 0!==t.length&&{url:t[0].attrs.href,text:"Listen"}},stripLinkFromHTML:function(html){var link=this.extractLink(html);return html.split(link.url)[2]},excerpt:function(t){console.log(t);var e=t.split("</p>");return(e[0].endsWith("</a>")?e[1].split(" ",50):e[0].split(" ",50)).join(" ")+"..."},detectiFrame:function(html){var t=new c.a(html).findAll("iframe"),e=[],r=[],i=1;if(t.length>0){var n,o=d(t);try{for(o.s();!(n=o.n()).done;){var l=n.value;if(l.attrs.src.includes("mixcloud")){var f={link:l.attrs.src,number:i};e.push(f)}else{var h={link:l.attrs.src,number:i};r.push(h)}i++}}catch(t){o.e(t)}finally{o.f()}r[0]&&(this.iframe=r[0]),e[0]&&(this.mixcloud=e[0]),this.html=this.stripiFrameFromHTML(this.post.html)}else this.html=this.post.html},stripiFrameFromHTML:function(html){return html.split("</iframe>")[1]}},mounted:function(){this.cta=this.extractLink(this.post.html),console.log(this.post),this.detectiFrame(this.post.html)},data:function(){return{cta:null,iframe:null,html:null,mixcloud:null}}},m=(r(290),r(14)),component=Object(m.a)(h,(function(){var t=this,e=t.$createElement,r=t._self._c||e;return r("article",{staticClass:"ph4 mb4"},[r("div",{staticClass:"aspect-ratio post relative",class:{"aspect-ratio--7x5":!t.mixcloud}},[t.post.feature_image&&t.tags?r("div",{staticClass:"tr right-0 absolute z-999 pr3"},t._l(t.post.tags,(function(e){return r("a",{staticClass:"tags",attrs:{href:"/tags/"+e.slug}},[t.tags?r("p",{staticClass:"bg-white black w-100 ph3 pv2"},[t._v(t._s(e.name))]):t._e()])})),0):t._e(),r("nuxt-link",{staticClass:"link",attrs:{to:{path:t.path+t.post.slug}}},[t.post.feature_image&&!t.mixcloud?r("div",{staticClass:"db bg-center cover aspect-ratio--object",style:{"background-image":"url("+t.post.feature_image+")","background-repeat":"no-repeat","background-position":"center"}},[t.iframe?r("iframe",{staticClass:"iframe w-100",attrs:{src:t.iframe.link}}):t._e()]):t._e(),t.post.feature_image&&t.mixcloud?r("div",{staticClass:"db verty w-100 z-999 flex justify-center items-center"},[r("iframe",{staticClass:"mixcloud",attrs:{src:t.mixcloud.link}})]):t._e()]),t.post.feature_image?t._e():r("div",{staticClass:"tc right-0 absolute z-999 pr3"},t._l(t.post.tags,(function(e){return r("a",{staticClass:"tags",attrs:{href:"/tags/"+e.slug}},[t.tags?r("p",{staticClass:"bg-white black w-100 ph3 pv2"},[t._v(t._s(e.name))]):t._e()])})),0),r("nuxt-link",{staticClass:"link",attrs:{to:{path:t.path+t.post.slug}}},[t.post.feature_image||t.mixcloud?t._e():r("div",{staticClass:"primary-bg db bg-center aspect-ratio--object"},[t.iframe?r("iframe",{staticClass:"iframe w-100",attrs:{src:t.iframe.link}}):t._e()]),t.post.feature_image?t._e():r("div",{staticClass:"z-max db flex align-center"},[t.mixcloud?r("iframe",{staticClass:"mixcloud w-100",attrs:{src:t.mixcloud.link}}):t._e()])])],1),r("nuxt-link",{staticClass:"link",attrs:{to:{path:t.path+t.post.slug}}},[r("div",{staticClass:"ph2 ph0-ns pb3 link"},[r("h2",{staticClass:"f3 mb0 fw5",class:{white:t.white}},[t._v(t._s(t.post.title)),r("p",{staticClass:"tl f4 fw4 underline",class:{white:t.white}},[t._v("Read more")])])])])],1)}),[],!1,null,"8df0df18",null);e.default=component.exports},295:function(t,e,r){t.exports=r.p+"img/wave.0080c77.svg"},327:function(t,e,r){var content=r(361);content.__esModule&&(content=content.default),"string"==typeof content&&(content=[[t.i,content,""]]),content.locals&&(t.exports=content.locals);(0,r(18).default)("c7049d9e",content,!0,{sourceMap:!1})},360:function(t,e,r){"use strict";r(327)},361:function(t,e,r){var n=r(17),o=r(289),l=r(295),c=n(!1),d=o(l);c.push([t.i,"a[data-v-528b95d4]{text-decoration:none}.logo[data-v-528b95d4]{width:60vw}.stable-post[data-v-528b95d4]{min-height:700px}.bg-about[data-v-528b95d4]{background-image:url("+d+");background-size:cover;background-repeat:repeat-y}a[data-v-528b95d4]:hover{border:1px solid #000}",""]),t.exports=c},390:function(t,e,r){"use strict";r.r(e);var n=r(2),o=(r(24),{components:{Post:r(292).default},mounted:function(){var t=this;return Object(n.a)(regeneratorRuntime.mark((function e(){return regeneratorRuntime.wrap((function(e){for(;;)switch(e.prev=e.next){case 0:return e.next=2,t.$store.dispatch("posts/fetchPosts");case 2:return e.next=4,t.$store.dispatch("posts/fetchTags");case 4:t.posts=t.$store.getters["posts/getPostsByTag"]("listen"),null==t.posts&&(t.posts=!1,t.error=!0);case 6:case"end":return e.stop()}}),e)})))()},data:function(){return{error:null,posts:null}}}),l=(r(360),r(14)),component=Object(l.a)(o,(function(){var t=this,e=t.$createElement,r=t._self._c||e;return r("div",{staticClass:"w-100 pb4"},[r("div",{staticClass:"w-100 center pt5",class:{"w-60-l":!t.$store.getters["settings/isMobile"]}},[r("h1",{staticClass:"tl pl4"},[t._v(" Listen")])]),r("div",{staticClass:"w-100"},[r("div",{staticClass:"center flex flex-wrap",class:{"w-80-l":!t.$store.getters["settings/isMobile"]}},t._l(t.posts,(function(t){return r("post",{key:t.id,staticClass:"pa2 tl w-50-ns w-100 ph4",attrs:{post:t,path:"/listen/",tags:!1,showDate:!1}})})),1)])])}),[],!1,null,"528b95d4",null);e.default=component.exports}}]);